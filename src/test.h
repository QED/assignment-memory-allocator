//
// Created by Alex on 03.01.2022.
//

#ifndef ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
#define ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
typedef enum{
    TEST_PASSED = 0,
    TEST_FAILED
} test;

test test_initialization();
test test_standard_memory_allocation();
test test_free_one_block();
test test_free_two_blocks();
test test_grow_heap();
test test_grow_heap_new_region();

#endif //ASSIGNMENT_MEMORY_ALLOCATOR_TEST_H
