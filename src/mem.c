#include <stdarg.h>
#define _DEFAULT_SOURCE
#include <unistd.h>
#include <stdio.h>
#include <stddef.h>
#include <stdlib.h>
#include <assert.h>

#include "mem_internals.h"
#include "mem.h"
#include "util.h"

void debug_block(struct block_header* b, const char* fmt, ... );
void debug(const char* fmt, ... );

extern inline block_size size_from_capacity( block_capacity cap );
extern inline block_capacity capacity_from_size( block_size sz );

static bool block_is_big_enough( size_t query, struct block_header* block ){
    return block->capacity.bytes >= query;
}
static size_t pages_count( size_t mem ){
    return mem / getpagesize() + ((mem % getpagesize()) > 0);
}
static size_t round_pages( size_t mem ){
    return getpagesize() * pages_count( mem );
}

// убрал static для тестов
void block_init( void* restrict addr, block_size block_sz, void* restrict next ) {
  *((struct block_header*)addr) = (struct block_header) {
    .next = next,
    .capacity = capacity_from_size(block_sz),
    .is_free = true
  };
}

//query - запрос
static size_t region_actual_size( size_t query ) {
    return size_max( round_pages( query ), REGION_MIN_SIZE );
}

extern inline bool region_is_invalid( const struct region* r );

//выделить доб. page
static void* map_pages(void const* addr, size_t length, int additional_flags) {
  return mmap( (void*) addr, length, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS | additional_flags , 0, 0 );
}//                                можно читать/ можно писать/----//----/отображение не резервируется/----/ fd / offset

/*MAP_PRIVATE                                       MAP_FIXED
    Изменения не передаются другим                      Аргумент addr интерпретируется как адрес памяти
    процессам и не влияют на отображенный объект
*/

/*  аллоцировать регион памяти и инициализировать его блоком */
static struct region alloc_region( void const* addr, const size_t query ) {
    size_t needed_size = region_actual_size( query );
    void* pntr_to_pages = map_pages(addr, needed_size, MAP_FIXED_NOREPLACE);//пытаемся выделить память вплотную
    struct region new_region;
    if( pntr_to_pages != MAP_FAILED ){
        new_region = (struct region){ pntr_to_pages, query, true };
    }else{//вплотную не получилось
        pntr_to_pages = map_pages(addr, needed_size, 0);//ну и ладно, выделим где получится
        if( pntr_to_pages == MAP_FAILED ) return REGION_INVALID;
        new_region = (struct region){ pntr_to_pages, query, false};
    }
    block_size block_sz = {query};
    block_init(pntr_to_pages, block_sz, NULL);
    return new_region;
}

static void* block_after( struct block_header const* block );

void* heap_init( size_t initial ) {
  const struct region region = alloc_region( HEAP_START, initial );
  if ( region_is_invalid(&region) ) return NULL;
  return region.addr;
}

#define BLOCK_MIN_CAPACITY 24
/*  --- Разделение блоков (если найденный свободный блок слишком большой )--- */

typedef struct block_search_result block_search_result;

//блок можно разделить
static bool block_splittable( struct block_header* restrict block, size_t query) {
  return block-> is_free && query + offsetof( struct block_header, contents ) + BLOCK_MIN_CAPACITY <= block->capacity.bytes;
}

//разделить если слишком большой
static bool split_if_too_big( struct block_header* block, size_t query ) {
    if(block_splittable(block, query)){
        block_size new_block_size = (block_size){.bytes = block->capacity.bytes - query};
        //вычисляем размер нового блока, если мы разделим старый большой
        if( new_block_size.bytes >= BLOCK_MIN_CAPACITY ){//если размер нового блока > min, то делим
            void* pntr_to_new_block = (void*)((uint8_t*)block + offsetof(struct block_header, contents) + query );
            block_init(pntr_to_new_block, new_block_size, block->next);
            block->next = pntr_to_new_block;
            block->capacity.bytes = query;
            return true;
        }//иначе не делим его
    }
    return false;
}

/*  --- Слияние соседних свободных блоков --- */
static void* block_after( struct block_header const* block ){
  return  (void*) (block->contents + block->capacity.bytes);
}

//идут ли блоки друг за другом
static bool blocks_continuous (struct block_header const* fst, struct block_header const* snd ){
  return (void*)snd == block_after(fst);
}

static bool mergeable(struct block_header const* restrict fst, struct block_header const* restrict snd){
  return fst->is_free && snd->is_free && blocks_continuous( fst, snd ) ;
}

static bool try_merge_with_next( struct block_header* block ) {
  if( block != NULL && mergeable(block, block->next) && block->next != NULL){
      block->capacity.bytes = block->capacity.bytes + size_from_capacity(block->next->capacity).bytes;
      block->next = block->next->next;
      return true;
  }
  return false;
}

/*  --- ... ecли размера кучи хватает --- */
struct block_search_result {
  enum {BSR_FOUND_GOOD_BLOCK, BSR_REACHED_END_NOT_FOUND, BSR_CORRUPTED, BSR_UNKNOWN_ERROR} type;
  struct block_header* block;
};

static struct block_search_result find_good_or_last ( struct block_header* restrict block, size_t sz ){
    if( block == NULL ){// error
        return (struct block_search_result){.type = BSR_CORRUPTED, block = NULL};
    }
    if( block->next == NULL && block->capacity.bytes < sz ){//last block
        return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND, .block = block};
    }
    if( block->capacity.bytes >= sz ){
        return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK, .block = block};
    }
    if( block->capacity.bytes < sz ){//пытаемся объединить со следующим, чтоб поместить sz
        while(true){
            bool merge_complete = try_merge_with_next(block);
            if( merge_complete && block_is_big_enough(sz, block)){
                return (struct block_search_result){.type = BSR_FOUND_GOOD_BLOCK, .block = block};
            }
            if(!merge_complete){//объединить не получилось -> нет подходящего
                return (struct block_search_result){.type = BSR_REACHED_END_NOT_FOUND, .block = block};
            }
        }
    }
    return (block_search_result){.type = BSR_UNKNOWN_ERROR, .block = NULL};
}

/*  Попробовать выделить память в куче начиная с блока `block` не пытаясь расширить кучу
 Можно переиспользовать как только кучу расширили. */
static struct block_search_result try_memalloc_existing ( size_t query, struct block_header* block ) {
    block_search_result good_block = find_good_or_last(block, query);
    if( good_block.type == BSR_FOUND_GOOD_BLOCK ){
        if( split_if_too_big(good_block.block, query) ){
            return good_block;
        }
    }
    return good_block;
}

//нарастить кучу //убрал static для тестов
struct block_header* grow_heap( struct block_header* restrict last, size_t query ) {
    struct block_header* new_block = (struct block_header*) heap_init(query);
    if( new_block == NULL ) return NULL;
    last->next = new_block;
    return new_block;
}


/*  Реализует основную логику malloc и возвращает заголовок выделенного блока */
static struct block_header* memalloc( size_t query, struct block_header* heap_start){
    size_t actually_size = size_max(query, BLOCK_MIN_CAPACITY);
    block_search_result target_block = find_good_or_last(heap_start, actually_size);
    if( target_block.type == BSR_FOUND_GOOD_BLOCK ){
        target_block = try_memalloc_existing(actually_size, target_block.block);
        return target_block.block;
    }else if( target_block.type == BSR_REACHED_END_NOT_FOUND ){
        target_block.block = grow_heap(target_block.block, actually_size);
        return target_block.block;
    }
    return NULL;
}

void* _malloc( size_t query ) {
  struct block_header* const addr = memalloc( query, (struct block_header*) HEAP_START );
  if (addr) return addr->contents;
  else return NULL;
}

//убрал static для тестов
struct block_header* block_get_header(void* contents) {
  return (struct block_header*) (((uint8_t*)contents)-offsetof(struct block_header, contents));
}

void _free( void* mem ) {
  if (!mem) return ;
  struct block_header* header = block_get_header( mem );
  header->is_free = true;
  while(try_merge_with_next(header));
}
