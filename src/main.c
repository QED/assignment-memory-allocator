//
// Created by Alex on 03.01.2022.
//
#include "test.h"

#include <stdio.h>

int main(){
    test check0 = test_initialization();
    test check1 = test_standard_memory_allocation();
    test check2 = test_free_one_block();
    test check3 = test_free_two_blocks();
    test check4 = test_grow_heap();
    test check5 = test_grow_heap_new_region();
    if(check0 == TEST_PASSED &&
       check1 == TEST_PASSED &&
       check2 == TEST_PASSED &&
       check3 == TEST_PASSED &&
       check4 == TEST_PASSED &&
       check5 == TEST_PASSED)
    {
        printf("ALL TEST PASSED \n");
    }else{
        printf("NOT ALL TEST PASSED!! \n");
    }
    return 0;
}
