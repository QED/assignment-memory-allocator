//
// Created by Alex on 03.01.2022.
//
#include "test.h"
#include "mem_internals.h"
#include "mem.h"
#include <stddef.h>
#include <stdbool.h>

/*
Придумайте тесты, показывающие работу аллокатора в важных случаях:
+Обычное успешное выделение памяти.
+Освобождение одного блока из нескольких выделенных.
+Освобождение двух блоков из нескольких выделенных.
+Память закончилась, новый регион памяти расширяет старый.
Память закончилась, старый регион памяти не расширить из-за другого
выделенного диапазона адресов, новый регион выделяется в другом месте.
*/ //TASKS

#define BLOCK_MIN_CAPACITY 24

test test_initialization(){
    printf("TEST 0: HEAP INITIALIZATION STARTED \n");
    size_t query_1 = 3 * 4096;
    size_t query_2 = 4096;
    void* addr_first_init = heap_init(query_1);
    if( addr_first_init == NULL ){
        printf("TEST 0.1 FAILED: heap_init returned NULL \n");
        return TEST_FAILED;
    }
    else printf("TEST 0.1 PASSED! \n");
    debug_heap(stdout, addr_first_init);

    void* addr_second_init = heap_init(query_2);
    if( addr_second_init == NULL ){
        printf("TEST 0.2 FAILED: heap_init returned NULL \n");
        return TEST_FAILED;
    }else printf("TEST 0.2 PASSED! \n");
    debug_heap(stdout, addr_second_init);
    printf("TEST 0 PASSED! \n");

    _free(addr_first_init);
    debug_heap(stdout, addr_first_init);
    _free(addr_second_init);
    debug_heap(stdout, addr_second_init);

    return TEST_PASSED;
}

static test test_sma_check(size_t query, size_t number){
    void* addr = _malloc(query);
    if( addr == NULL ){
        printf("TEST 1.%zu FAILED: _malloc returned NULL \n", number);
        return TEST_FAILED;
    }
    struct block_header* header_addr = block_get_header(addr);
    if( header_addr->is_free == true ){
        printf("TEST 1.%zu FAILED: header_addr1 is free", number);
        return TEST_FAILED;
    }
    if( header_addr->capacity.bytes != BLOCK_MIN_CAPACITY ||
        header_addr->capacity.bytes != query ){
        printf("TEST 1.%zu FAILED: capacity if wrong, expected %zu or %zu get %zu", number,
               query, BLOCK_MIN_CAPACITY, header_addr->capacity.bytes);
        return TEST_FAILED;
    }
    debug_heap(stdout, addr);
    _free(addr);
    debug_heap(stdout, addr);
    return TEST_PASSED;
}
test test_standard_memory_allocation(){
    printf("TEST 1: MEMORY ALLOCATION STARTED \n");
    size_t query1 = 0;
    size_t query2 = 24;
    size_t query3 = 300;
    if(test_sma_check(query1, 1) != TEST_PASSED) return TEST_FAILED;
    if(test_sma_check(query2, 2) != TEST_PASSED) return TEST_FAILED;
    if(test_sma_check(query3, 3) != TEST_PASSED) return TEST_FAILED;
    printf("TEST 1 PASSED! \n");
    return TEST_PASSED;
}

test test_free_one_block(){
    printf("TEST 2: BLOCK FREE STARTED \n");
    size_t query1 = 24;
    size_t query2 = 300;
    size_t query3 = 1000;
    void* addr1 = _malloc(query1);
    void* addr2 = _malloc(query2);
    void* addr3 = _malloc(query3);
    struct block_header* header_addr = block_get_header(addr2);
    _free(addr2);
    debug_heap(stdout, addr1);
    debug_heap(stdout, addr2);
    debug_heap(stdout, addr3);
    if( !header_addr->is_free ){
        printf("TEST 2.1 FAILED: is_free not eq true \n");
        return TEST_FAILED;
    }
    printf("TEST 2 PASSED! \n");
    _free(addr1);
    _free(addr3);
    return TEST_PASSED;
}

test test_free_two_blocks(){
    printf("TEST 3: BLOCK's FREE STARTED \n");
    size_t query1 = 24;
    size_t query2 = 300;
    size_t query3 = 1000;
    void* addr1 = _malloc(query1);
    void* addr2 = _malloc(query2);
    void* addr3 = _malloc(query3);
    struct block_header* header_addr_1 = block_get_header(addr1);
    struct block_header* header_addr_2 = block_get_header(addr2);
    _free(addr1);
    _free(addr2);
    debug_heap(stdout, addr1);
    debug_heap(stdout, addr2);
    debug_heap(stdout, addr3);
    if( !header_addr_1->is_free ){
        printf("TEST 3.1 FAILED: is_free not eq true \n");
        return TEST_FAILED;
    }
    if( !header_addr_2->is_free ){
        printf("TEST 3.2 FAILED: is_free not eq true \n");
        return TEST_FAILED;
    }
    printf("TEST 3 PASSED! \n");
    _free(addr3);
    return TEST_PASSED;
}

test test_grow_heap(){
    printf("TEST 4: GROW HEAP STARTED \n");
    size_t query1 = 10;
    size_t query2 = 1000;
    void* addr_to_block = heap_init(query1);
    block_size bs_query1 = {query1};
    block_init(addr_to_block, bs_query1, NULL);
    struct block_header* new_block = grow_heap(addr_to_block, query2);
    if( new_block == NULL ){
        printf("TEST 4 FAILED: grow_heap returned NULL \n");
        return TEST_FAILED;
    }
    if( ((struct block_header*)addr_to_block)->next != new_block ){
        printf("TEST 4 FAILED: addr_to_block->next != new_block \n");
        return TEST_FAILED;
    }
    debug_heap(stdout, addr_to_block);
    printf("TEST 4 PASSED! \n");
    _free(addr_to_block);
    return TEST_PASSED;
}

test test_grow_heap_new_region(){
    printf("TEST 5: GROW HEAP IN OTHER PLACE STARTED \n");
    size_t query1 = 24;
    size_t query2 = 100;
    size_t query3 = 1000;
    void* addr1 = _malloc(query1);
    void* addr2 = _malloc(query2);
    void* addr3 = _malloc(query3);
    struct block_header* last = block_get_header(addr3);
    while(last->next) last->next = last->next->next;

    size_t query4 = 10000;
    void* stopper = last + size_from_capacity(last->capacity).bytes;
    stopper = mmap(stopper, query4, PROT_READ | PROT_WRITE, MAP_PRIVATE |
                                    MAP_ANONYMOUS |  MAP_FIXED_NOREPLACE, 0, 0 );
    if( stopper == MAP_FAILED ){
        printf("TEST 5 FAILED: failed to allocate memory for stopper \n");
        return TEST_FAILED;
    }
    block_init(stopper, (block_size){query4}, NULL);
    debug_heap(stdout, stopper);

    size_t query5 = 1000;
    void* new_block = _malloc(query5);
    struct block_header* new_block_header = block_get_header(new_block);
    if( new_block_header == stopper ){
        printf("TEST 5 FAILED: new_block allocated close \n");
        return TEST_FAILED;
    }
    debug_heap(stdout, new_block);
    printf("TEST 5 PASSED! \n");
    _free(stopper);
    _free(new_block);
    _free(addr3);
    _free(addr2);
    _free(addr1);
    return TEST_PASSED;
}

